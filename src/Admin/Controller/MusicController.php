<?php

namespace Admin\Controller;

use Admin\Form\Type\GenreType;
use Admin\Form\Type\MusicType;
use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package Admin\Controller
 * @Route("/musics")
 */
class MusicController extends Controller
{
    /**
     * Создание трека
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     * @Route("/add", name="music_add")
     */
    public function createAction(Request $request)
    {
        $music = new Music();

        $form = $this->createForm(MusicType::class, $music);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $music = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($music);
            $em->flush();

            return $this->redirectToRoute('musics');
        }

        return $this->render("admin/musics/edit.html.twig", array(
            'form' => $form->createView()
        ));
    }

    /**
     * Изменение трека
     *
     * @param Request $request
     * @param $id
     * @return Response
     *
     * @Route("/{id}/edit", name="music_edit", requirements={"id"="\d+"}))
     */
    public function updateAction(Request $request, $id)
    {
        $music = $this->getDoctrine()->getRepository(Music::class)->find($id);

        if (!$music)
            throw $this->createNotFoundException('Трек не существует.');

        $fileLinkOld = $music->getUrl();

        $form = $this->createForm(MusicType::class, $music);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $music = $form->getData();

            $file = $music->getMusicFile();
            if ($file instanceof File) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('tracks_directory'), $fileName);
                $music->setUrl($fileName);

                if ($fileLinkOld) {
                    try {
                        $fs = new Filesystem();
                        $fs->remove($this->getParameter('tracks_directory') . "/" . $fileLinkOld);
                    } catch (IOExceptionInterface  $e) {
                        dump("Ошибка удаления файла");
                    }
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($music);
            $em->flush();

            return $this->redirectToRoute('musics');
        }

        return $this->render("admin/musics/edit.html.twig", array(
            'form' => $form->createView(),
            'music' => $music
        ));
    }

    /**
     * Удаление Трека
     *
     * @param $id
     * @return Response
     *
     * @Route("/{id}/delete", name="music_remove", requirements={"id"="\d+"}))
     */
    public function deleteAction($id)
    {
        $music = $this->getDoctrine()->getRepository(Music::class)->find($id);

        if (!$music)
            throw $this->createNotFoundException('Трек не существует.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($music);
        $em->flush();

        return $this->redirectToRoute('musics');
    }
}