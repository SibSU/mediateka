<?php

namespace Admin\Controller;

use Admin\Form\Type\AuthorType;
use Admin\Form\Type\GenreType;
use App\Entity\Author;
use App\Entity\Genre;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthorController
 * @package Admin\Controller
 * @Route("/artists")
 */
class AuthorController extends Controller
{
    /**
     * Создание Исполнителя
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     * @Route("/add", name="artist_add")
     */
    public function createAction(Request $request)
    {
        $artist = new Author();

        $form = $this->createForm(AuthorType::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $artist = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($artist);
            $em->flush();

            return $this->redirect($this->generateUrl('artist_show', array('id' => $artist->getId())));
        }

        return $this->render("admin/artists/edit.html.twig", array(
            'form' => $form->createView()
        ));
    }

    /**
     * Изменение Исполнителя
     *
     * @param Request $request
     * @param $id
     * @return Response
     *
     * @Route("/{id}/edit", name="artist_edit", requirements={"id"="\d+"}))
     */
    public function updateAction(Request $request, $id)
    {
        $artist = $this->getDoctrine()->getRepository(Author::class)->find($id);

        if (!$artist)
            throw $this->createNotFoundException('Исполнителя не существует.');

        $form = $this->createForm(AuthorType::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $artist = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($artist);
            $em->flush();

            return $this->redirect($this->generateUrl('artist_show', array('id' => $artist->getId())));
        }

        return $this->render("admin/artists/edit.html.twig", array(
            'form' => $form->createView(),
            'artist' => $artist
        ));
    }

    /**
     * Удаление Исполнителя
     *
     * @param $id
     * @return Response
     *
     * @Route("/{id}/delete", name="artist_remove", requirements={"id"="\d+"}))
     * @throws DBALException
     */
    public function deleteAction($id)
    {
        $artist = $this->getDoctrine()->getRepository(Author::class)->find($id);

        if (!$artist)
            throw $this->createNotFoundException('Исполнителя не существует.');

        if (count($artist->getTracks()) > 0)
            throw new DBALException('У исполнителя есть песни!');

        $em = $this->getDoctrine()->getManager();
        $em->remove($artist);
        $em->flush();

        return $this->redirectToRoute('artists');
    }
}