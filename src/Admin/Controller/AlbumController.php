<?php

namespace Admin\Controller;

use Admin\Form\Type\AlbumType;
use Admin\Form\Type\AuthorType;
use Admin\Form\Type\GenreType;
use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Genre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AlbumController
 * @package Admin\Controller
 * @Route("/albums")
 */
class AlbumController extends Controller
{
    /**
     * Создание альбома
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     * @Route("/add", name="album_add")
     */
    public function createAction(Request $request)
    {
        $album = new Album();

        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $album = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirect($this->generateUrl('album_show', array('id' => $album->getId())));
        }

        return $this->render("admin/albums/edit.html.twig", array(
            'form' => $form->createView()
        ));
    }

    /**
     * Изменение альбома
     *
     * @param Request $request
     * @param $id
     * @return Response
     *
     * @Route("/{id}/edit", name="album_edit", requirements={"id"="\d+"}))
     */
    public function updateAction(Request $request, $id)
    {
        $album = $this->getDoctrine()->getRepository(Album::class)->find($id);

        if (!$album)
            throw $this->createNotFoundException('Альбома не существует.');

        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $album = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirect($this->generateUrl('album_show', array('id' => $album->getId())));
        }

        return $this->render("admin/albums/edit.html.twig", array(
            'form' => $form->createView(),
            'album' => $album
        ));
    }

    /**
     * Удаление Исполнителя
     *
     * @param Request $request
     * @param $id
     * @return Response
     *
     * @Route("/{id}/delete", name="album_remove", requirements={"id"="\d+"}))
     */
    public function deleteAction(Request $request, $id)
    {
        $album = $this->getDoctrine()->getRepository(Album::class)->find($id);

        if (!$album)
            throw $this->createNotFoundException('Альбома не существует.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($album);
        $em->flush();

        return $this->redirectToRoute('albums');
    }
}