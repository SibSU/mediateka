<?php

namespace Admin\Controller;

use Admin\Form\Type\GenreType;
use App\Entity\Genre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package Admin\Controller
 * @Route("/genres")
 */
class GenreController extends Controller
{
    /**
     * Создание жанра
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     * @Route("/add", name="genre_add")
     */
    public function createAction(Request $request)
    {
        $genre = new Genre();

        $form = $this->createForm(GenreType::class, $genre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $genre = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genre);
            $em->flush();

            return $this->redirect($this->generateUrl('genre_show', array('id' => $genre->getId())));
        }

        return $this->render("admin/genres/edit.html.twig", array(
            'form' => $form->createView()
        ));
    }

    /**
     * Изменение жанра
     *
     * @param Request $request
     * @param $id
     * @return Response
     *
     * @Route("/{id}/edit", name="genre_edit", requirements={"id"="\d+"}))
     */
    public function updateAction(Request $request, $id)
    {
        $genre = $this->getDoctrine()->getRepository(Genre::class)->find($id);

        if (!$genre)
            throw $this->createNotFoundException('Жанр не существует.');

        $form = $this->createForm(GenreType::class, $genre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $genre = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genre);
            $em->flush();

            return $this->redirect($this->generateUrl('genre_show', array('id' => $genre->getId())));
        }

        return $this->render("admin/genres/edit.html.twig", array(
            'form' => $form->createView(),
            'genre' => $genre
        ));
    }

    /**
     * Удаление жанра
     *
     * @param $id
     * @return Response
     *
     * @Route("/{id}/delete", name="genre_remove", requirements={"id"="\d+"}))
     */
    public function deleteAction($id)
    {
        $genre = $this->getDoctrine()->getRepository(Genre::class)->find($id);

        if (!$genre)
            throw $this->createNotFoundException('Жанр не существует.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($genre);
        $em->flush();

        return $this->redirectToRoute('genres');
    }
}