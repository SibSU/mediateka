<?php

namespace Admin\Form\Type;

use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MusicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'attr' => array(
                    'placeholder' => 'Введите название...',
                ),
                'required' => true
            ))
            ->add('authors', EntityType::class, array(
                'label' => 'Автор или авторы',
                'class' => Author::class,
                'placeholder' => 'Выберите автора или авторов...',
                'by_reference' => false,
                'multiple' => true,
            ))
            ->add('musicFile', FileType::class, [
                'label' => 'Трек',
                'attr' => array(
                    'type' => 'file',
                ),
                'required' => false,
            ])
            ->add('genre', EntityType::class, array(
                'label' => 'Жанр',
                'class' => Genre::class,
                'placeholder' => 'Выберите жанр...',
                'required' => false
            ))
            ->add('album', EntityType::class, array(
                'label' => 'Альбом',
                'class' => Album::class,
                'placeholder' => 'Введите альбом...',
                'required' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Сохранить'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Music::class,
        ));
    }
}