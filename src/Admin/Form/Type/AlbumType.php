<?php

namespace Admin\Form\Type;

use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'attr' => array(
                    'placeholder' => 'Введите название...',
                ),
                'required' => true
            ))
            ->add('author', EntityType::class, array(
                'label' => 'Исполнитель',
                'class' => Author::class,
                'placeholder' => 'Выберите исполнителя...',
                'required' => true
            ))
            ->add('musics', EntityType::class, array(
                'label' => 'Треки',
                'class' => Music::class,
                'placeholder' => 'Введите треки альбома...',
                'by_reference' => false,
                'multiple' => true,
                'required' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Сохранить'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Album::class,
        ));
    }
}