<?php

namespace Admin\Form\Type;

use App\Entity\Genre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'attr' => array(
                    'placeholder' => 'Введите название...',
                ),
                'required' => true
            ))
            ->add('desc', TextareaType::class, array(
                'label' => 'Описание',
                'attr' => array(
                    'placeholder' => 'Введите описание...',
                ),
                'required' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Сохранить'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Genre::class,
        ));
    }
}