<?php
namespace App\Repository;

use App\Entity\Author;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use PDO;

class AuthorRepository extends EntityRepository
{
    public function findPopular(int $limit)
    {
        $artists = new ArrayCollection();

        try {
            $sth = $this->getEntityManager()
                ->getConnection()
                ->prepare("CALL procedurePopularAuthor(". $limit .")");
            $sth->execute();

            while ($row = $sth->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
                $author = new Author();
                $author->setId($row[0]);
                $author->setName($row[1]);
                $author->setDesc($row[2]);
                $artists->add($author);
            }
        } catch (\Exception $ex) {
            dump($ex);
        }

        return $artists;
    }
}