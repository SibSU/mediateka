<?php
namespace App\Repository;

use App\Entity\Genre;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use PDO;

class GenreRepository extends EntityRepository
{
    public function findPopular(int $limit)
    {
        $genres = new ArrayCollection();

        try {
            $sth = $this->getEntityManager()
                ->getConnection()
                ->prepare("CALL procedurePopularGenre(". $limit .")");
            $sth->execute();

            while ($row = $sth->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
                $genre = new Genre();
                $genre->setId($row[0]);
                $genre->setName($row[1]);
                $genre->setDesc($row[2]);
                $genres->add($genre);
            }
        } catch (\Exception $ex) {
            dump($ex);
        }

        return $genres;
    }
}