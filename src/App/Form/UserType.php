<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 14.04.2018
 * Time: 15:46
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 *
 * Date: 05.05.2018
 * @package Admin\Form\Type
 * @author <yuriyyurinskiy@yandex.ru>
 */
class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Имя пользователя',
                'attr' => array(
                    'placeholder' => 'Введите имя пользователя...',
                ),
                'required' => true
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Пароль',
                'attr' => array(
                    'placeholder' => 'Введите пароль...',
                ),
                'required' => true
            ))
            ->add('save', SubmitType::class, array('label' => 'Зарегистрироваться'));;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}