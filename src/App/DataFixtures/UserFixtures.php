<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $encoder = new MessageDigestPasswordEncoder(
            'sha512',
            true,
            10
        );

        // Создаем роли
        $role = new Role();
        $role->setName('ROLE_ADMIN');
        $manager->persist($role);

        $role_user = new Role();
        $role_user->setName('ROLE_USER');
        $manager->persist($role_user);

        // Создаем пользователей
        $user = new User();
        $user->setUsername('admin');
        $user->setSalt(md5(time()));
        $user->setPassword($encoder->encodePassword('admin', $user->getSalt()));
        $user->addRole($role);
        $manager->persist($user);

        $user1 = new User();
        $user1->setUsername('user');
        $user1->setSalt(md5(time()));
        $user1->setPassword($encoder->encodePassword('user', $user1->getSalt()));
        $user1->addRole($role_user);
        $manager->persist($user1);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}