<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package Controller
 * @Route("/artists")
 */
class AuthorController extends Controller
{
    /**
     * Страница жанров
     *
     * @Route("/", name="artists")
     */
    public function indexAction() {
        $artists = $this->getDoctrine()->getRepository(Author::class)->findAll();

        return $this->render("artists/index.html.twig", array(
            'artists' => $artists
        ));
    }

    /**
     * Страница просмотра жанра
     *
     * @param $id
     * @return Response
     *
     * @Route("/{id}/show", name="artist_show", requirements={"id"="\d+"}))
     */
    public function showAction($id) {
        $artist = $this->getDoctrine()->getRepository(Author::class)->find($id);
        if (!$artist)
            throw $this->createNotFoundException('Исполнителя не существует.');
        $musics = $artist->getTracks();
        $albums = $artist->getAlbums();

        return $this->render("artists/show.html.twig", array(
            'artist' => $artist,
            'musics' => $musics,
            'albums' => $albums
        ));
    }
}