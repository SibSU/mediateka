<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AlbumController
 * @package Controller
 * @Route("/albums")
 */
class AlbumController extends Controller
{
    /**
     * Страница альбомов
     *
     * @Route("/", name="albums")
     */
    public function indexAction() {
        $albums = $this->getDoctrine()->getRepository(Album::class)->findAll();

        return $this->render("albums/index.html.twig", array(
            'albums' => $albums
        ));
    }

    /**
     * Страница просмотра альбома
     *
     * @param $id
     * @return Response
     *
     * @Route("/{id}/show", name="album_show", requirements={"id"="\d+"}))
     */
    public function showAction($id) {
        $album = $this->getDoctrine()->getRepository(Album::class)->find($id);
        if (!$album)
            throw $this->createNotFoundException('Альбома не существует.');
        $musics = $this->getDoctrine()->getRepository(Music::class)->findBy(['album' =>  $album], [], 10);

        return $this->render("albums/show.html.twig", array(
            'album' => $album,
            'musics' => $musics
        ));
    }
}