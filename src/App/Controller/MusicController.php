<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package Controller
 * @Route("/musics")
 */
class MusicController extends Controller
{
    /**
     * Страница треки
     *
     * @Route("/", name="musics")
     */
    public function indexAction() {
        $musics = $this->getDoctrine()->getRepository(Music::class)->findAll();

        return $this->render("musics/index.html.twig", array(
            'musics' => $musics
        ));
    }
}