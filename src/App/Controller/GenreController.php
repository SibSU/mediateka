<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package Controller
 * @Route("/genres")
 */
class GenreController extends Controller
{
    /**
     * Страница жанров
     *
     * @Route("/", name="genres")
     */
    public function indexAction() {
        $genres = $this->getDoctrine()->getRepository(Genre::class)->findAll();

        return $this->render("genres/index.html.twig", array(
            'genres' => $genres
        ));
    }

    /**
     * Страница просмотра жанра
     *
     * @param $id
     * @return Response
     *
     * @Route("/{id}/show", name="genre_show", requirements={"id"="\d+"}))
     */
    public function showAction($id) {
        $genre = $this->getDoctrine()->getRepository(Genre::class)->find($id);
        if (!$genre)
            throw $this->createNotFoundException('Жанр не существует.');
        $tracks = $this->getDoctrine()->getRepository(Music::class)->findBy(['genre' => $genre], [], 10);

        return $this->render("genres/show.html.twig", array(
            'genre' => $genre,
            'musics' => $tracks
        ));
    }
}