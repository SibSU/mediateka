<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Music;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 * @package Controller
 */
class PageController extends Controller
{
    /**
     * Главная страница
     *
     * @Route("/", name="home")
     */
    public function indexAction() {
        $musics = $this->getDoctrine()->getRepository(Music::class)->findBy([],[],4);
        $albums = $this->getDoctrine()->getRepository(Album::class)->findBy([],[],7);
        $artists = $this->getDoctrine()->getRepository(Author::class)->findPopular(3);
        $genres = $this->getDoctrine()->getRepository(Genre::class)->findPopular(3);

        return $this->render("home.html.twig", array(
            'musics' => $musics,
            'albums' => $albums,
            'artists' => $artists,
            'genres' => $genres
        ));
    }
}