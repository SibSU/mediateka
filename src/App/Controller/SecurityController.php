<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @param string|null $success
     * @return Response
     *
     * @Route("login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils) {

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        $success = $request->get('success');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'success'       => $success,
        ));
    }

    /**
     * @Route("logout", name="logout")
     */
    public function logoutAction() {

    }

    /**
     * @Route("/register", name="register")
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $role = $this->getDoctrine()->getRepository(Role::class)->findOneBy(['name' => 'ROLE_USER']);

            $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
            $user->setSalt(md5(time()));
            $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
            $user->addRole($role);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('login', array('success' => 'Вы зарегистрировались успешно!'));
        }

        return $this->render('register.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Создать пользователя',
        ));
    }
}