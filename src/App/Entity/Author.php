<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="App\Repository\AuthorRepository")
 */
class Author
{
    /**
     * @var int Идентификатор
     *
     * @ORM\Column(name="IDau", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string Имя
     *
     * @ORM\Column(name="auName", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var string Описание
     *
     * @ORM\Column(name="gDesc", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Album", mappedBy="author")
     */
    private $albums;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Music", mappedBy="authors");
     */
    private $tracks;

    /**
     * Author constructor.
     */
    public function __construct() {
        $this->albums = new ArrayCollection();
        $this->tracks = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDesc(): ?string
    {
        return $this->desc;
    }

    public function setDesc(?string $desc): self
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setAuthor($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
            // set the owning side to null (unless already changed)
            if ($album->getAuthor() === $this) {
                $album->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Music[]
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Music $track): self
    {
        if (!$this->tracks->contains($track)) {
            $this->tracks[] = $track;
            $track->addAuthor($this);
        }

        return $this;
    }

    public function removeTrack(Music $track): self
    {
        if ($this->tracks->contains($track)) {
            $this->tracks->removeElement($track);
            $track->removeAuthor($this);
        }

        return $this;
    }
}
