<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Music
 *
 * @ORM\Table(name="music", indexes={
 *     @ORM\Index(name="music_ibfk_1", columns={"IDge"}),
 *     @ORM\Index(name="music_ibfk_2", columns={"IDal"})
 * })
 * @ORM\Entity
 */
class Music
{
    /**
     * @var int Идентификатор
     *
     * @ORM\Column(name="IDmc", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string Название композиции
     *
     * @ORM\Column(name="cName", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var File
     *
     * @Assert\File(
     *     maxSize="12M",
     *     maxSizeMessage="Загружаемый файл слишком большой ({{ size }} {{ suffix }}). Максимальный размер равен {{ limit }} {{ suffix }}",
     *     mimeTypes = {"application/octet-stream", "audio/mpeg", "audio/mp3"},
     * )
     */
    private $musicFile;

    /**
     * @ORM\Column("url", type="string", length=255)
     *
     * @var string
     */
    private $url;

    /**
     * @var Genre Жанр
     *
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="musics")
     * @ORM\JoinColumn(name="IDge", referencedColumnName="IDge", onDelete="SET NULL")
     */
    private $genre;

    /**
     * @var Album Альбом
     *
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="musics")
     * @ORM\JoinColumn(name="IDal", referencedColumnName="IDal", nullable=true, onDelete="SET NULL")
     */
    private $album;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Author", inversedBy="tracks")
     * @ORM\JoinTable(name="list",
     *  joinColumns={
     *      @ORM\JoinColumn(name="IDmc", referencedColumnName="IDmc")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="IDau", referencedColumnName="IDau")
     *  })
     */
    private $authors;

    /**
     * Music constructor.
     */
    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMusicFile(): ?File
    {
        return $this->musicFile;
    }

    /**
     * @param File|UploadedFile $music
     */
    public function setMusicFile(?File $music = null)
    {
        $this->musicFile = $music;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    public function setAlbum(?Album $album): self
    {
        $this->album = $album;

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->contains($author)) {
            $this->authors->removeElement($author);
        }

        return $this;
    }
}
