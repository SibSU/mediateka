<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     name="users",
 *     options={"comment":"Таблица пользователей"}
 * )
 * @UniqueEntity(
 *     "username",
 *     message="Введенное имя пользователя уже используется."
 * )
 */
class User implements UserInterface
{
    /**
     * @var int Индентификатор пользователя
     *
     * @ORM\Id()
     * @ORM\Column(
     *     name="id_user",
     *     type="integer",
     *     unique=true,
     *     options={"comment":"ИД пользователя"}
     * )
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string Логин пользователя
     *
     * @ORM\Column(
     *     name="username",
     *     type="string",
     *     unique=true,
     *     length=50,
     *     options={"comment":"Имя пользователя"}
     * )
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 4,
     *      minMessage = "Должно быть не менее {{ limit }} символов",
     *      max = 50,
     *      maxMessage = "Должно быть не более {{ limit }} символов"
     * )
     */
    private $username;

    /**
     * @var string Хеш пароля
     *
     * @ORM\Column(name="password", type="string", length=128, options={"comment":"Пароль пользователя"})
     * @Assert\Length(
     *     min=6,
     *     minMessage="Должно быть не менее {{ limit }} символов"
     * )
     */
    private $password;

    /**
     * @var string Соль
     *
     * @ORM\Column(name="salt", type="string", options={"comment":"Соль пароля"})
     */
    private $salt;

    /**
     * @var ArrayCollection Роли пользователя
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="users_roles",
     *     joinColumns={
     *         @ORM\JoinColumn(name="user_id", referencedColumnName="id_user")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="role_id", referencedColumnName="id_role")
     *     }
     * )
     */
    private $roles;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getRoles()
    {
        $roles = array();

        foreach ($this->getRolesCollection() as $role) {
            array_push($roles, $role->getName());
        }

        return $roles;
    }

    /**
     * @return ArrayCollection
     */
    public function getRolesCollection() {
        return $this->roles;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }
}
