<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Genre
 *
 * @ORM\Table(name="genre")
 * @ORM\Entity(repositoryClass="App\Repository\GenreRepository")
 */
class Genre
{
    /**
     * @var int Идентификатор
     *
     * @ORM\Column(name="IDge", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string Название жанра
     *
     * @ORM\Column(name="gName", type="string", length=30, nullable=false)
     * @Assert\NotNull()
     * @Assert\Length(
     *     min=3,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     max=30,
     *     maxMessage="Должно быть не более {{ limit }} символов"
     * )
     */
    private $name;

    /**
     * @var string Описание
     *
     * @ORM\Column(name="gDesc", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Music", mappedBy="genre")
     */
    private $musics;

    public function __construct()
    {
        $this->musics = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDesc(): ?string
    {
        return $this->desc;
    }

    public function setDesc(?string $desc): self
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * @return Collection|Music[]
     */
    public function getMusics(): Collection
    {
        return $this->musics;
    }

    public function addMusic(Music $music): self
    {
        if (!$this->musics->contains($music)) {
            $this->musics[] = $music;
            $music->setGenre($this);
        }

        return $this;
    }

    public function removeMusic(Music $music): self
    {
        if ($this->musics->contains($music)) {
            $this->musics->removeElement($music);
            // set the owning side to null (unless already changed)
            if ($music->getGenre() === $this) {
                $music->setGenre(null);
            }
        }

        return $this;
    }


}
