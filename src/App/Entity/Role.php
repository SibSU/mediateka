<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     name="roles",
 *     options={"comment":"Таблица ролей"}
 * )
 * @UniqueEntity(
 *     "name",
 *     message="Введенное системное имя роли уже используется."
 * )
 */
class Role
{
    /**
     * @var int Идентификатор роли
     *
     * @ORM\Id()
     * @ORM\Column(
     *     name="id_role",
     *     type="integer",
     *     unique=true,
     *     options={"comment":"ИД роли"}
     * )
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string Системное имя роли
     *
     * @ORM\Column(
     *     name="name",
     *     type="string",
     *     unique=true,
     *     length=20,
     *     options={"comment":"Системное имя роли"}
     * )
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=6,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     max=20,
     *     maxMessage="Должно быть не более {{ limit }} символов"
     * )
     */
    private $name;

    /**
     * @var ArrayCollection Пользователи данной ролью
     *
     * @ORM\ManyToMany(
     *     targetEntity="User",
     *     mappedBy="roles"
     * )
     */
    private $users;

    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addRole($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeRole($this);
        }

        return $this;
    }
}
